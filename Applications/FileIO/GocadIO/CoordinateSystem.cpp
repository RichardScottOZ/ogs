/**
 * \file
 * \copyright
 * Copyright (c) 2012-2022, OpenGeoSys Community (http://www.opengeosys.org)
 *            Distributed under a Modified BSD License.
 *              See accompanying file LICENSE.txt or
 *              http://www.opengeosys.org/project/license
 */

#include "CoordinateSystem.h"

#include <boost/algorithm/string/trim.hpp>
#include <boost/tokenizer.hpp>
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <string>
#include "BaseLib/Logging.h"

namespace FileIO
{
namespace Gocad
{
std::string parseName(std::string const& str)
{
    std::string name;
    std::size_t const start = str.find_first_of('\"');
    if (start != std::string::npos)
    {
        std::size_t const end = str.find_last_of('\"');
        name = str.substr(start + 1, end - start - 1);
    }
    else
    {
        name = str.substr(str.find_first_of(' '), str.length());
    }
    boost::algorithm::trim(name);
    return name;
}

bool CoordinateSystem::parse(std::istream& in)
{
    std::string line;  // string used for reading a line
    std::string richard;
    boost::char_separator<char> sep("-;| \"");

    std::getline(in, line);  // NAME name
    boost::tokenizer<boost::char_separator<char>> tok(line, sep);
    auto it(tok.begin());
    if (*it != "NAME")
    {
        return false;
    }
    name = parseName(line);
    projection = "";
    datum = "";

    while (std::getline(in, line))
    {
        tok.assign(line);
        it = tok.begin();
        //for (const auto& t: tok) {
           //std::cout << "TOK " << t << std::endl;
        //while ( it != tok.end())
        //{
           
           //std::cout << "TOK " <<  *it << std::endl;
           //++it;
        //}
           
            //std::cout << t;        
        //std::cout << line;
        //printf("linecheck %s\n", line);
        //int n = line.length();
 
        // declaring character array
        //char char_array[n + 1];
 
        // copying the contents of the
        // string to char array
        //strcpy(char_array, line.c_str());
        //printf("linecheck %s", char_array);   
        //for (int i = 0; i < n; i++)
            //std::cout << char_array[i];
        //boost::tokenizer<boost::char_separator<char>> cathtok(line, sep);
        //cathtok.assign(line);
        //it = cathtok.begin();
        //auto it(cathtok.begin());
        //std::cout << *it << "\n";
        richard = *it;
        boost::algorithm::trim(richard);
        if (*it == "AXIS_NAME")
        {
            //int n = line.length();
            //std::cout << "line crap " <<  n << "\n";
            //char char_array[n + 1];
            //strcpy(char_array, line.c_str());
            //for (int i  = 0; i < n; i++)
               //std::cout << char_array[i];

            //continue;
            //std::cout << t;
            ++it;
            axis_name_u = *it;
            //std::cout << t;
            ++it;
            axis_name_v = *it;
            //std::cout << t;
            ++it;
            axis_name_w = *it;
            std::cout << "before u";
            //std::cout << axis_name_u;
            //std::cout << axis_name_v;
            std::cout << "before w";
            std::cout << *it;
            //std::cout << axis_name_w;
            std::cout << "sb AXIS_NAME ";
        }
        else if (*it == "AXIS_UNIT")
        {
            //++it;
            axis_unit_u = *it;
            //++it;
            axis_unit_v = *it;
            //++it;
            axis_unit_w = *it;
            std::cout  << "sb AXIS_UNIT ";
        }
        else if (*it == "ZPOSITIVE")
        {
            ++it;
            if (*it == "Depth")
            {
                z_positive = ZPOSITIVE::Depth;
            }
            else
            {
                z_positive = ZPOSITIVE::Elevation;
            }
            std::cout << "sb ZPOSITIVE ";
        }
        else if (*it == "PROJECTION")
        {
            //break;
            //std::cout << "sb PROJECTION ";
            projection = parseName(line);
            std::cout << "sb PROJECTION ";
            //break;
        }
        else if (*it == "DATUM")
        {
            //break;
            //std::cout << "sb DATUM ";
            datum = parseName(line);
            std::cout << "sb DATUM";
            //break;
        }
        else if (richard == "END_ORIGINAL_COORDINATE_SYSTEM")
    
        //else if (richard.substr(0,8) == "END_ORIG") 
        {
            std::cout << "sb END COORD";
            //break;
            return true;
        }
        else
        //if ( 1 == 2)
        {
            WARN("CoordinateSystem::parse() - Unknown keyword found: {:s}",
                 line);
        }
    }
    ERR("Error: Unexpected end of file.");
    return false;
    //} //end of tok loop
}

std::ostream& operator<<(std::ostream& os, CoordinateSystem const& c)
{
    os << "Gocad CoordinateSystem " << c.name
       << "\nAxis names: " << c.axis_name_u << ", " << c.axis_name_v << ", "
       << c.axis_name_w << "\nAxis units: " << c.axis_unit_u << ", "
       << c.axis_unit_v << ", " << c.axis_unit_w << "\nZ-orientation: ";
    if (c.z_positive == FileIO::Gocad::CoordinateSystem::ZPOSITIVE::Depth)
    {
        os << "downwards";
    }
    else
    {
        os << "upwards";
    }
    os << "\n";
    return os;
}

}  // end namespace Gocad
}  // end namespace FileIO
